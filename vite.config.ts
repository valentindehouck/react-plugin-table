import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path'
import * as packageJson from './package.json'
import dts from 'vite-plugin-dts'
import { libInjectCss } from 'vite-plugin-lib-inject-css'

export default defineConfig({
  build: {
    lib: {
      entry: path.resolve(__dirname, 'src/index.ts'),
      name: 'react-plugin-table',
      fileName: (format) => `index.${format}.js`,
    },
    rollupOptions: {
      external: [...Object.keys(packageJson.peerDependencies)],
      output: {
        globals: {
          react: 'React',
        },
      },
    },
    emptyOutDir: true,
  },
  plugins: [react(), libInjectCss(), dts({
    exclude: '**/*.stories.ts',
  })],
})
