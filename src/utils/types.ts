type ColumnType = {
  label: string
  isSortable?: boolean
  cell: string
}

type RowType = {
  id: string | number
  [key: string]: string | number | Date
}

type SortingConfig = {
  key: string | null
  direction: 'asc' | 'desc'
}

export type { ColumnType, RowType, SortingConfig }
