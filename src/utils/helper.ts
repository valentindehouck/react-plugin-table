import { SortingConfig, RowType } from './types.ts'

export function pagination(current: number, last: number, delta: number) {
  if (last === 1) {
    return [1]
  }

  const left = current - delta
  const right = current + delta + 1
  const range = []

  for (let i = 1; i <= last; i++) {
    if (i == 1 || i == last || (i >= left && i < right)) {
      if ((i === left && i > 2) || (i === right - 1 && i < last - 1)) {
        range.push('...')
      }

      range.push(i)
    }
  }

  return range
}

export function filterData(searchTerm: string, data: RowType[]) {
  return data.filter((row) =>
    Object.values(row).some((value) =>
      value instanceof Date
        ? value
          .toLocaleDateString()
          .toLowerCase()
          .trim()
          .includes(searchTerm.toLowerCase().trim())
        : value
          .toString()
          .toLowerCase()
          .trim()
          .includes(searchTerm.toLowerCase().trim()),
    ),
  )
}

export function sortData(sorting: SortingConfig, data: RowType[]) {
  return [...data].sort((a, b) => {
    if (sorting.key === null) {
      return 0
    }
    if (a[sorting.key] < b[sorting.key]) {
      return sorting.direction === 'asc' ? -1 : 1
    }
    if (a[sorting.key] > b[sorting.key]) {
      return sorting.direction === 'asc' ? 1 : -1
    }
    return 0
  })
}
