import { forwardRef, HTMLProps } from 'react'
import classes from '../../index.module.scss'

const Checkbox = forwardRef<HTMLInputElement, HTMLProps<HTMLInputElement>>(
  ({ id, checked, children, ...props }, ref) => {
    return (
      <>
        <input type="checkbox" id={id} checked={checked} ref={ref} {...props} />
        <label className={classes.sr_only} htmlFor={id}>
          {children}
        </label>
      </>
    )
  },
)

export default Checkbox
