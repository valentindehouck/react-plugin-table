import { Dispatch, SetStateAction, useEffect, useState } from 'react'
import { pagination } from '../../utils/helper.ts'
import classes from './Pagination.module.scss'

type PaginationType = {
  rowsPerPage: number
  totalRecords: number
  currentPage: number
  changePage: Dispatch<SetStateAction<number>>
}

function Pagination({
                      rowsPerPage,
                      totalRecords,
                      currentPage,
                      changePage,
                    }: PaginationType) {
  const [totalPages, setTotalPages] = useState<number>(0)
  const [pagesToDisplay, setPagesToDisplay] = useState<
    (string | number)[] | null
  >(null)

  useEffect(() => {
    setTotalPages(Math.ceil(totalRecords / rowsPerPage))

    if (totalRecords && totalPages > 1) {
      setPagesToDisplay(pagination(currentPage, totalPages, 2))
    } else {
      setPagesToDisplay(null)
    }
  }, [currentPage, rowsPerPage, totalPages, totalRecords])

  if (!pagesToDisplay) {
    return
  }

  return (
    <div>
      {pagesToDisplay.map((pageNumber, index) => {
        if (typeof pageNumber === 'string') {
          return (
            <button key={index} className={`${classes.page} ${classes.ellipsis}`} role="button"
                    aria-pressed="false">&#8230;</button>
          )
        }

        return (
          <button
            key={index}
            value={pageNumber}
            className={
              currentPage === pageNumber
                ? `${classes.active_page} ${classes.page}`
                : classes.page
            }
            onClick={() => changePage(pageNumber)}
          >
            {pageNumber}
          </button>
        )
      })}
    </div>
  )
}

export default Pagination
