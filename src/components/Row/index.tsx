import { ReactNode } from 'react'

type RowProps = {
  label: string
  children: ReactNode
  className: string | undefined
}

function Row({ label, children, className }: RowProps) {
  return <td data-title={label} className={className}>{children}</td>
}

export default Row
