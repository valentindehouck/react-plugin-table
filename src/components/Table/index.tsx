import { Pagination, Search, Row, Checkbox, Column, Select } from '../index.ts'
import { useState, useMemo, ChangeEvent } from 'react'
import { filterData, sortData } from '../../utils/helper.ts'
import { ColumnType, RowType, SortingConfig } from '../../utils/types.ts'
import classes from './Table.module.scss'

type TableProps = {
  columns: ColumnType[]
  rows: RowType[] | undefined | null
  label: string
  description?: string
  isFiltered?: boolean
  rowsPerPage?: number
  rowsPerPageOptions?: number[]
  sortField?: string | null
  checkboxSelection?: boolean
}

function Table({
                 columns,
                 rows,
                 label,
                 description,
                 isFiltered = false,
                 rowsPerPage = 10,
                 sortField = null,
                 rowsPerPageOptions = [10, 20, 50, 80, 100],
                 checkboxSelection = false,
               }: TableProps) {
  const [currentPage, setCurrentPage] = useState<number>(1)
  const [currentRowsPerPage, setCurrentRowsPerPage] =
    useState<number>(rowsPerPage)
  const [selected, setSelected] = useState<string[]>([])
  const [searchTerm, setSearchTerm] = useState<string>('')
  const [sorting, setSorting] = useState<SortingConfig>({
    key: sortField ? sortField : null,
    direction: 'asc',
  })
  const [currentTotalRows, setCurrentTotalRows] = useState<number>(rows ? rows.length : 0)

  const currentTableData = useMemo(() => {
    if (!rows) {
      return null
    }

    const filteredData = filterData(searchTerm, rows)
    const sortedData = sortData(sorting, filteredData)

    setCurrentTotalRows(sortedData.length)

    const indexFirstElement = (currentPage - 1) * currentRowsPerPage
    const indexLastElement = indexFirstElement + currentRowsPerPage

    return sortedData.slice(indexFirstElement, indexLastElement)
  }, [searchTerm, rows, sorting, currentPage, currentRowsPerPage])

  const select = ({ target }: ChangeEvent<HTMLInputElement>) => {
    if (!rows || !currentTableData) {
      return
    }

    const { id, checked } = target
    setSelected(
      checked
        ? [...selected, id]
        : (selected) => selected.filter((item) => item !== id),
    )
  }

  const selectAll = ({ target }: ChangeEvent<HTMLInputElement>) => {
    if (!rows || !currentTableData) {
      return
    }

    const { checked } = target
    const idCheckbox = rows.map((item) => `${item.id}`)
    setSelected(checked ? idCheckbox : [])
  }

  const sort = (cell: string) => {
    if (!rows || !currentTableData) {
      return
    }

    if (sorting.key === cell && sorting.direction === 'desc') {
      setSorting({
        key: null,
        direction: 'asc',
      })
    } else {
      setSorting({
        key: cell,
        direction:
          cell === sorting.key
            ? sorting.direction === 'asc'
              ? 'desc'
              : 'asc'
            : 'asc',
      })
    }
  }

  return (
    <>
      {rows && isFiltered ? (
        <Search searchTerm={searchTerm} setSearchTerm={setSearchTerm} />
      ) : null}
      <div className={classes.table_container}>
        <table className={classes.table} role="grid"
               aria-colcount={checkboxSelection ? columns.length + 1 : columns.length}
               aria-rowcount={currentTableData?.length}>
          <caption>
            {label} {rows && <span className={classes.badge}>{rows.length} items</span>}
            {description ? <p>{description}</p> : null}
          </caption>
          <thead>
          <tr>
            {checkboxSelection && rows ? (
              <th className={classes.checkbox} role="columnheader" aria-colindex={1}>
                <Checkbox
                  id="all"
                  checked={selected.length === rows.length}
                  onChange={selectAll}
                >
                  Select All
                </Checkbox>
              </th>
            ) : null}
            {columns.map(({ label, cell, isSortable }, index) => {
              return (
                <Column
                  key={cell}
                  sorting={sorting}
                  label={label}
                  cell={cell}
                  isSortable={currentTableData ? isSortable : false}
                  onClick={() => (isSortable ? sort(cell) : null)}
                  aria-colindex={checkboxSelection ? index + 2 : index + 1}
                />
              )
            })}
          </tr>
          </thead>
          <tbody>
          {rows ?
            currentTableData && currentTableData.length > 0 ?
              currentTableData.map((element, index) => {
                return (
                  <tr
                    key={index}
                    className={
                      selected.includes(`${element.id}`)
                        ? `${classes.selected}`
                        : undefined
                    }
                  >
                    {checkboxSelection ? (
                      <td key={index}>
                        <Checkbox
                          key={index}
                          id={`${element.id}`}
                          checked={selected.includes(`${element.id}`)}
                          onChange={select}
                        >
                          Select {element.id}
                        </Checkbox>
                      </td>
                    ) : null}
                    {columns.map(({ cell, label }) => {
                      return (
                        <Row key={cell} label={label} className={sorting.key === cell ? classes.sorting : undefined}>
                          {currentTableData[index][cell]
                            ? currentTableData[index][cell] instanceof Date
                              ? currentTableData[index][cell].toLocaleDateString()
                              : currentTableData[index][cell]
                            : '-'}
                        </Row>
                      )
                    })}
                  </tr>
                )
              })
              :
              <tr>
                <td className={classes.no_result} colSpan={checkboxSelection ? columns.length + 1 : columns.length}>
                  No results... Try expanding the search
                </td>
              </tr>
            :
            <tr>
              <td className={classes.no_data} colSpan={checkboxSelection ? columns.length + 1 : columns.length}>
                No data available in table
              </td>
            </tr>
          }
          </tbody>
        </table>
      </div>
      {rows &&
        <div className={classes.footer}>
          <Select defaultValue={currentRowsPerPage} setRowsPerPage={setCurrentRowsPerPage}
                  rowsPerPageOptions={rowsPerPageOptions}
                  setCurrentPage={setCurrentPage} />
          <Pagination
            rowsPerPage={currentRowsPerPage}
            totalRecords={currentTotalRows}
            currentPage={currentPage}
            changePage={setCurrentPage}
          />
        </div>
      }
    </>
  )
}

export default Table
