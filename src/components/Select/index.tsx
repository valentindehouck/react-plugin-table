import commonClasses from '../../index.module.scss'
import classes from './Select.module.scss'
import { Dispatch, SetStateAction } from 'react'
import './Select.module.scss'

type SelectProps = {
  defaultValue: number
  setRowsPerPage: Dispatch<SetStateAction<number>>
  setCurrentPage: Dispatch<SetStateAction<number>>
  rowsPerPageOptions: number[]
}

function Select({ defaultValue, setRowsPerPage, setCurrentPage, rowsPerPageOptions }: SelectProps) {

  if (!rowsPerPageOptions.includes(defaultValue)) {
    setRowsPerPage(rowsPerPageOptions[0])
  }

  return (
    <>
      <select
        id="rowsPerPage"
        className={classes.select}
        defaultValue={defaultValue}
        onChange={({ target }) => {
          setRowsPerPage(parseInt(target.value))
          setCurrentPage(1)
        }}
      >
        {rowsPerPageOptions.map((option, index) => (
          <option key={index} value={option}>{option}</option>
        ))}
      </select>
      <label htmlFor="rowsPerPage" className={commonClasses.sr_only}>
        Rows per page
      </label>
    </>
  )
}

export default Select