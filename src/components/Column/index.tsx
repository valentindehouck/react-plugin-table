import { ColumnType, SortingConfig } from '../../utils/types.ts'
import { MouseEventHandler } from 'react'
import classes from './Column.module.scss'

interface ColumnProps extends ColumnType {
  sorting: SortingConfig
  onClick: MouseEventHandler
}

function Column({ label, isSortable, sorting, cell, onClick, ...props }: ColumnProps) {
  const { direction, key } = sorting
  const ariaSort = isSortable ? key === cell && direction === 'asc' ? 'ascending' : key === cell && direction === 'desc' ? 'descending' : 'none' : undefined

  return (
    <th onClick={onClick} className={isSortable ? `${classes.sortable} ${classes.column}` : classes.column}
        role="columnheader"
        aria-sort={ariaSort} {...props}>
      <span>{label}</span>
      {isSortable ?
        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
          {key === cell ?
            <>
              <path
                d="M10.1921 3.23047L13.9065 7.68785C14.3408 8.20891 13.9702 9 13.292 9L6.70803 9C6.02976 9 5.65924 8.20892 6.09346 7.68785L9.80794 3.23047C9.90789 3.11053 10.0921 3.11053 10.1921 3.23047Z"
                fill="currentColor" opacity={direction === 'asc' ? '0.3' : undefined} />
              <path
                d="M9.80794 16.7695L6.09346 12.3121C5.65924 11.7911 6.02976 11 6.70803 11L13.292 11C13.9702 11 14.3408 11.7911 13.9065 12.3121L10.1921 16.7695C10.0921 16.8895 9.90789 16.8895 9.80794 16.7695Z"
                fill="currentColor" opacity={direction === 'desc' ? '0.3' : undefined} />
            </>
            :
            <>
              <path
                d="M10.1921 3.23047L13.9065 7.68785C14.3408 8.20891 13.9702 9 13.292 9L6.70803 9C6.02976 9 5.65924 8.20892 6.09346 7.68785L9.80794 3.23047C9.90789 3.11053 10.0921 3.11053 10.1921 3.23047Z"
                fill="currentColor" opacity="0.3" />
              <path
                d="M9.80794 16.7695L6.09346 12.3121C5.65924 11.7911 6.02976 11 6.70803 11L13.292 11C13.9702 11 14.3408 11.7911 13.9065 12.3121L10.1921 16.7695C10.0921 16.8895 9.90789 16.8895 9.80794 16.7695Z"
                fill="currentColor" opacity="0.3" />
            </>
          }
        </svg> : null}
    </th>
  )
}

export default Column
