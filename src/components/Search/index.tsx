import { Dispatch, SetStateAction } from 'react'
import classes from './Search.module.scss'
import genericsClasses from '../../index.module.scss'

type SearchProps = {
  searchTerm: string
  setSearchTerm: Dispatch<SetStateAction<string>>
}

function Search({ searchTerm, setSearchTerm }: SearchProps) {
  return (
    <div className={classes.search}>
      <label className={genericsClasses.sr_only} htmlFor="search">
        Search...
      </label>
      <input
        id="search"
        placeholder="Search..."
        type="search"
        value={searchTerm}
        onChange={({ target }) => setSearchTerm(target.value)}
      />
      <svg
        width="21"
        height="20"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M9.66667 15.8333c3.68193 0 6.66663-2.9847 6.66663-6.66663 0-3.6819-2.9847-6.66667-6.66663-6.66667C5.98477 2.5 3 5.48477 3 9.16667c0 3.68193 2.98477 6.66663 6.66667 6.66663ZM18 17.5l-3.625-3.625"
          stroke="#000"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    </div>
  )
}

export default Search
