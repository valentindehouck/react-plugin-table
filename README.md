# react-plugin-table

A react plugin for implementing data tables

[![NPM downloads](https://img.shields.io/npm/dt/react-plugin-table.svg)](https://www.npmjs.com/package/react-plugin-table)
[![NPM version](https://badge.fury.io/js/react-plugin-table.svg)](https://badge.fury.io/js/vue-js-modal)
[![Package size](https://img.shields.io/bundlephobia/min/react-plugin-table)](https://bundlephobia.com/package/react-plugin-table)

[![NPM](https://img.shields.io/badge/npm-CB3837?style=for-the-badge&logo=npm&logoColor=white)](https://www.npmjs.com/)
[![Storybook](https://img.shields.io/badge/-Storybook-FF4785?style=for-the-badge&logo=storybook&logoColor=white)](https://react-plugin-table-valentindehouck-ced0af1b98033ce0e4f4ffaa4269.gitlab.io/)

## Installation

```
npm install react-plugin-table
```

## Usage

```js
import { Table } from 'react-plugin-table';
```

Next, declare the columns your table will need in an array of objects :

```js
  const columns = [
  {
    label: 'First column',
    cell: 'firstColumn',
    isSortable: true,
  },
  {
    label: 'Second column',
    cell: 'secondColumn'
  },
  {
    label: 'Third column',
    cell: 'thirdColumn'
  }
];
```

Declare the contents of your table with an array containing an object corresponding to each row. If you don't want any
cells to appear, simply don't declare the column.

**Warning: a unique id must be declared in each object**

An example of data :

```js
  const rows = [
  {
    id: '1',
    firstColumn: 'First Line',
    secondColumn: 'First Line',
    thirdColumn: 'First Line',
  },
  {
    id: '2',
    firstColumn: 'Second Line',
    secondColumn: 'Second Line',
    thirdColumn: 'Second Line',
  },
  {
    id: '3',
    firstColumn: 'Third Line',
    secondColumn: 'Third Line',
    thirdColumn: 'Third Line',
  },
];
```

To finish, use the table as follows:

```js
  return (
  <Table columns={columns} label="Table label" description="Table description" rows={rows}/>
);
```

### Features

- **Column sorting :** The columns having the *sortable* boolean to true will have the sort option active, which allows
  them to be ascendingly sorted on the first click on the header of the column, and descendingly sorted on second click
  of the column.
- **Search :** A table with *isFiltered* boolean with true value, a search input will be displayed.
- **Pagination :** A pagination system is implemented to present tables with many entries in a simple way. You can
  select the amount of data to be displayed per page using a selector.
- **Responsive :** The component is responsive and can be displayed on desktops and small screens without scrolling.

### Future developments

#### Essential

- [ ] Make the colours used in css customisable
- [ ] Finish implementing accessible elements (aria attributes and keyboard navigation management)
- [ ] Be able to add action buttons to a list with selection

#### For later

- [ ] Add unit tests
- [ ] Add support for defining column groups
- [ ] Add a deltaPagination props to the component to customise the number of pages required before and after the
  active (currently only 2 possible)
- [ ] Allow the use of ajax calls for searching and pagination

### Summary Props

#### Table

| Name               | Type     | Default | Description                                                                        |
|--------------------|----------|---------|------------------------------------------------------------------------------------|
| columns            | Array    |         | For more information, please refer to the summary `Table.columns`                  |
| rows               | Array    | -       | For more information, please refer to the summary `Table.rows`                     |
| label              | String   | -       | Title for the table                                                                |
| description        | String   | -       | Description for the table                                                          |
| isFiltered         | Boolean  | false   | A search input will be displayed with `true` value                                 |
| rowsPerPage        | Number   | 10      | Number of items per page desired when the component is loaded                      |
| rowsPerPageOptions | Number[] | null    | Define the list of select options for the number of items to be displayed per page |
| sortField          | String   | null    | Specify the column to be sorted by default when the component is loaded            |
| checkboxSelection  | Boolean  | false   | Each row becomes selectable with a checkbox and a ‘Select all’ for the column      |

### Table.columns

| Name       | Type    | Default | Description                                          |
|------------|---------|---------|------------------------------------------------------|
| label      | String  |         | Content of a cell                                    |
| isSortable | Boolean |         | Is the column sortable ?                             |
| cell       | String  |         | key associated with the contents of the desired cell |

### Table.rows

| Name | Type                     | Default | Description       |
|------|--------------------------|---------|-------------------|
| id   | String \| Number         |         | Id of a row       |
| -    | String \| Number \| Date |         | Content of a cell |

## Licence

